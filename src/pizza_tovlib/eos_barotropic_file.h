#ifndef EOSBAROTROPICFILE_H
#define EOSBAROTROPICFILE_H

#include "pizza_tovlib/eos_barotropic.h"
#include "pizza_tovlib/unitconv.h"
#include <string>
#include <fstream>
#include <istream>



namespace EOS_Barotropic {

/// Load EOS from stream
eos_1p load_eos_1p_stream(
  std::istream &inputstream, ///< Input stream (eg. file or string) to read from
  const Pizza::units& u=Pizza::units::geom_meter()  ///< Unit system to be used by EOS
);

/// Legacy: Load EOS From file.
inline eos_1p load_eos_1p(
  std::string fname,  ///< Name of file to load from
  const Pizza::units& u=Pizza::units::geom_meter()  ///< Unit system to be used by EOS
) {
  std::ifstream f(fname.c_str());
  return load_eos_1p_stream(f, u);
}


}

#endif

