#ifndef PIZZAS_FUNNY_ERROR
#define PIZZAS_FUNNY_ERROR

namespace Pizza {

// coming from PizzaBase/src/mesh.h
class error: public std::exception
{
  const char* msg;
  public:
  error(const char* msg_) : msg(msg_) {}
  virtual const char* what() const throw()
  {
    return msg;
  }
  static void unless(bool c, const char* m)
  {
    if (!c) throw error(m);
  }
  static void incase(bool c, const char* m)
  {
    unless(!c,m);
  }
};

} // ns

#endif /* PIZZAS_FUNNY_ERROR */
