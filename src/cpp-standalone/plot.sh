#!/bin/bash

# A mini gnuplot wrapper in order to print out the
# tables created by the demonstration program.

for outasc in out_*.asc; do
	# make output directory
	outdir=plots_${outasc%.asc}
	[ -d $outdir ] && rm -r $outdir
	mkdir $outdir

	# read columns
	for col in $(head -n1 $outasc); do
		echo "plot $outasc, column $col"
		echo "set term png; set output '$outdir/${col}.png'; plot '$outasc' using 'x':'$col' w lp" | gnuplot
	done


done
