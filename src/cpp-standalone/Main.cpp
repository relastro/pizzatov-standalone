/**
 * A standalone application written in C++ to demonstrate the pizza_tovfront
 * pointwise cartesian interpolation interface to the PizzaTOV initial data
 * code.
 * 
 * This program differes from the "tovstar" program from Pizza that it does not
 * only dump information about stars but mimics the usage in some evolution
 * code (like ExaHyPE).
 * 
 * Compile and run like:
 * 
 * $ cd ..
 * $ make cpp_demonstrator
 * $ ./demonstrator
 *
 **/

#include "pizza_tovfront/pointwise_tov.h"
#include <stdio.h>
#include <algorithm>

int main(int argc, char** argv) {
	using namespace Pizza::TOV;
	
	pointwise_tov id;

	// set some parameters
	id.star_crmd = 7.9056e17;
	
	// set some mapping information
	const int adm_len = id.adm.count; // double foo[const int]: C++11.
	const int hydro_len = id.hydro.count;
	double *Q = new double[adm_len + hydro_len];
	double *ADM = &Q[0];
	id.adm.setStartFrom(0);
	double *Hydro = &Q[adm_len];
	id.hydro.setStartFrom(adm_len);
	
	// skip to funny stuff which the standard EOS doesnt have
	id.hydro.temp = id.hydro.disabled;
	id.hydro.efrac = id.hydro.disabled;

	fprintf(stderr, "Running Preparation code...\n");
	id.compute_star();
	
	// zero variables since some indices are skipped
	std::fill_n(ADM, adm_len, 0.0);
	std::fill_n(Hydro, hydro_len, 0.0);
	
	FILE* fADM = fopen("out_adm.asc", "w");
	FILE* fHydro = fopen("out_hydro.asc", "w");
	if(!fADM) { printf("Could not open fADM.\n"); return 1; }
	if(!fHydro) { printf("Could not open fHydro.\n"); return 1; }

	const int dim = 3, points = 100;
	double xmin = -20.0, xmax = 20.0;
	int i[dim];
	double pos[dim]={0.0};
	fprintf(stderr, "Now interpolate at %d positions\n", points);

	fputs("x y z ", fADM); fputs("x y z ", fHydro);
	for(int p=0; p<adm_len; p++) fprintf(fADM, "ADM%d ", p);
	for(int p=0; p<hydro_len; p++) fprintf(fHydro, "Hydro%d ", p);
	fputs("\n", fADM); fputs("\n", fHydro);
	
	for(i[0]=0;i[0]<points;i[0]++) {
	pos[0] = xmin + i[0] *  (xmax - xmin) / points;
	//for(i[1]=0;i[1]<points;i[1]++) {
	//pos[1] = i[1] * xyzmax / points;
	//for(i[2]=0;i[2]<points;i[2]++) {
	//pos[2] = i[2] * xyzmax / points;
		
		id.initial_data(pos, Q);
		
		// and/or check for NaNs.
		for(int p=0; p<adm_len;p++) {
			if(!(ADM[p]==ADM[p])) {
				printf("NaN at %d. position\n", p);
				abort();
			}
		}
		for(int p=0; p<hydro_len;p++) {
			if(!(Hydro[p]==Hydro[p])) {
				printf("NaN at %d. position\n", p);
				abort();
			}
		}
		
		// Write out everything in individual files:
		for(int d=0; d<dim; d++) {
			fprintf(fADM, "%f ", pos[d]);
			fprintf(fHydro, "%f ", pos[d]);
		}
		for(int p=0; p<adm_len;p++) fprintf(fADM, "%e ", ADM[p]);
		for(int p=0; p<hydro_len;p++) fprintf(fHydro, "%e ", Hydro[p]);
		fputs("\n", fADM); fputs("\n", fHydro);
		
		// or print out some data
		//printf("%5e ", pos[0]);
		//for(int p=0;p<Qlen;p++) printf("%5e ", Q[p]); printf("\n");
	}//}} // end of 3-loop
	
	printf("Done\n");
	return 0;
}
