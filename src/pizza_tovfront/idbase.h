#ifndef PIZZA_IDBASE_TOVFRONT
#define PIZZA_IDBASE_TOVFRONT

#include <string>
#include "pizza_tovfront/adapt.h"
#include "pizza_tovlib/eos_polytropic.h"


namespace Pizza {
namespace TOV {

using EOS_Barotropic::eos_polytrope;
using EOS_Barotropic::eos_1p;
using EOS_Barotropic::eos_cold;
	
/**
 * Mimics the PizzaIDBase thorn with its parameters and possibilities.
 * 
 * Important: If you want to read your EOS from a file or a string, you have to call the
 * read_eos() function afterwards.
 * 
 * Thus, it is recommended you use the functions read_eos_from_file() etc. functions
 * instead which do it for you.
 * 
 **/
struct idbase : public adapt {
	
	// "old" crappy parameter interface which mimics Cactus parameters:
	enum class eos_from_keyword {
		PizzaFile,   // "Read from a file in PIZZA format. The path is given by the eos_file parameter"
		PizzaString, // "Read from a given Cactus parameter which holds the contents of a PIZZA format EOS."
		Nowhere      // "Do not set an initial EOS at all."
	} eos_from; ///< "Source of initial data EOS"

	std::string eos_string; ///< String containing the initial data EOS: An EOS in valid Pizza format
	std::string eos_file; ///< "Filename containing initial data EOS"
	
	void read_eos();
	
	// "new" method interface which does everything for you:
	void read_eos_from_file(std::string& filename);
	void read_eos_from_string(std::string& eos_pizza_string);

	// the actual EOS, of course you can touch it on your own
	eos_1p eos;

	// or just start with some example EOS.
	idbase();
	eos_1p get_some_bu0_eos_to_start_with();
	
}; // class

} // ns
} // ns

#endif /* PIZZA_IDBASE_TOVFRONT */
