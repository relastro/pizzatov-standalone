#include "pizza_tovfront/idbase.h"
#include "pizza_tovlib/eos_barotropic_file.h"
#include <stdio.h>
#include <sstream>

using namespace Pizza;
using namespace Pizza::TOV;


void idbase::read_eos() {
	// from PizzaIDBase_Startup:
	bool from_file = (eos_from == eos_from_keyword::PizzaFile);
	bool from_string = (eos_from == eos_from_keyword::PizzaString);
	bool donotset = (eos_from == eos_from_keyword::Nowhere);
  
	if(from_file)   read_eos_from_file(eos_file);
	if(from_string) read_eos_from_string(eos_string);
}

void idbase::read_eos_from_file(std::string& filename) {
	printf("Loading EOS from file '%s'.", eos_file.c_str());
	eos = EOS_Barotropic::load_eos_1p(eos_file, internal_units);
}

void idbase::read_eos_from_string(std::string& eos_pizza_string) {
	std::stringstream eos_stream(eos_string);
	printf("Loading EOS from string (%ld characters).", eos_string.length());
	eos = EOS_Barotropic::load_eos_1p_stream(eos_stream, internal_units);
}

idbase::idbase() {
	eos = get_some_bu0_eos_to_start_with();
}

eos_1p idbase::get_some_bu0_eos_to_start_with() {
	const int poly_n = 1;
	const double poly_rmd = 6.176e+18 / internal_units.density();
	const double rmd_max  = 1e100;
	return eos_1p(new eos_polytrope(poly_n, poly_rmd, rmd_max, "BU0_to_start"));
}
