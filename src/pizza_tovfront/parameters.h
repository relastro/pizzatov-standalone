#ifndef PIZZA_TOVFRONT_PARAMETERS
#define PIZZA_TOVFRONT_PARAMETERS

#include <string>

namespace Pizza {
namespace TOV {

/**
 * As always, moved from the Cactus parameters file.
 * 
 * Note that Pizza internally uses the SI unit system.
 * 
 **/
struct parameters {
	std::string out_dir;

	// Decisions what ID to set: These typical Cactus parameters do not apply
	// for the pointwise interface.
	// Instead, everything is computed and stored in the idvars struct
	// and users can copy what they need.

	// in parameter language:
	// std::string initial_data, initial_lapse, initial_shift, initial_temperature,initial_Y_e;
	// in Pizza language:
	// bool set_shift, set_lapse, set_temp, set_efrac;

	std::string idtype; ///<Number of NS to create"
	// "TOV" :: "Single neutron star"
	// "BNS" :: "Two neutron stars (constraint violating!)"

	double star_gm1_cut; ///< "Artificial surface criterion"

	//#############################################################################
	// Parameters used only for the single neutron star case
	//#############################################################################
	double star_crmd; ///< "Central rest mass density", in SI units

	int pert_l; ///< "Perturbation with Y_lm", range: 0..10
	int pert_m; ///< "Perturbation with Y_lm", range: 0..10
	int pert_n; ///< "Perturbation with nth overtone", range: 0..10
	
	double pert_amp; ///<"Perturbation amplitude", range  -0.9:0.9
	double kick_amp; ///< "Radial kick amplitude"
	
	const bool has_pert() const { return pert_amp != 0; }
	const bool has_kick() const { return kick_amp != 0; }

	//#############################################################################
	// Parameters used only for the double neutron star case
	//#############################################################################
	double star_rhoc[2]; ///< "Central density of each star"

	double star_posx[2]; ///< "x-position of each star"
	double star_posy[2]; ///< "y-position of each star"
	double star_posz[2]; ///< "z-position of each star"
	double star_velx[2]; ///< "x-velocity of each star". Velocities in (-1,1)
	double star_vely[2]; ///< "y-velocity of each star"
	double star_velz[2]; ///< "z-velocity of each star"

	parameters() {
		idtype = "TOV";
		
		star_gm1_cut = 0;
		star_crmd = 7.9056e17;
		pert_l = 0;
		pert_m = 0;
		pert_n = 1;
		
		// by default, no pert, no kick
		pert_amp = 0;
		kick_amp = 0;
		
		star_rhoc[0] = 7.9056e1;
		star_rhoc[1] = 7.9056e1;
		star_posx[0] = 0;
		star_posx[1] = 0;
		star_posy[0] = 0;
		star_posy[1] = 0;
		star_posz[0] = 0;
		star_posz[1] = 0;
		star_velx[0] = 0;
		star_velx[1] = 0;
		star_vely[0] = 0;
		star_vely[1] = 0;
		star_velz[0] = 0;
		star_velz[1] = 0;
	}

}; // class

} // ns
} // ns

#endif /* PIZZA_TOVFRONT_PARAMETERS */
