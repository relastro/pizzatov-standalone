#ifndef PIZZA_TOVFRONT_IDVARS
#define PIZZA_TOVFRONT_IDVARS

#include "pizza_tovfront/vectornames.h"
#include "pizza_tovlib/smatrix.h"
#include "pizza_tovlib/eos_barotropic.h"

namespace Pizza {
namespace TOV {

using EOS_Barotropic::eos_1p;
using EOS_Barotropic::eos_cold;


/**
 * The Pizza'ish interface to the hydro/spacetime data at some specific point.
 * 
 **/
class idvars {
public:
  mats_l glo;
  mats_l klo;
  double rmd, sed, press;
  vec_u vel;
  double florentz;
  double vsqr() const {return glo.contract(vel);}
  double florentz_from_vel() const {return 1.0/sqrt(1.0-vsqr());}

  double lapse;
  vec_u shift;

  /**
   * Copy contents hold by this idvars instance to a vector of primitive variables.
   *
   * In the Cactus version of Pizza, this is done by lazy versions of the
   * smatrix.h classes which directly bind the data fields. However, in the end,
   * data is copied anyway since there is still the idvars instance and later data
   * copied to the lazy shadow.
   * 
   * If we want to avoid copying data at all, the whole structure should work with
   * kind of pointer variants of the smatrix.h data types.
   * 
   * However this is initial data, so the copying does not really matter.
   * 
   * The implementation here allows you to provide the ADM/Hydro mapping dynamically,
   * in principle even allows you to switch the EOS when copying stuff. However,
   * look at the pointwise_tov::initial_data(double*,double*) function to get a
   * more friendly interface to this.
   * 
   **/
  void copy(double* targetPrimitives, const ADMBase& adm, const HydroBase& hydro, const eos_1p& eos);
};

/**
 * We can dump the contents of an idvars, helpful for debugging.
 **/
std::ostream& operator<<(std::ostream &s,const idvars &id);

} // ns TOV
} // ns Pizza

#endif /* PIZZA_TOVFRONT_IDVARS */
