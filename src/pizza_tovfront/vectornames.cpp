#include "pizza_tovfront/vectornames.h"

// if you add another field to a base, make sure you add:
//  a) your new field = disabled in the constructor
//  b) the guard in isempty()
//  c) setstartFrom = an increasing number.
//  d) The count field in the header file to the maximum number.

Pizza::TOV::ADMBase::ADMBase() {
	gxx     = disabled;
	gxy     = disabled;
	gxz     = disabled;
	gyy     = disabled;
	gyz     = disabled;
	gzz     = disabled;
	kxx     = disabled;
	kxy     = disabled;
	kxz     = disabled;
	kyy     = disabled;
	kyz     = disabled;
	kzz     = disabled;
	alp     = disabled;
	shift1  = disabled;
	shift2  = disabled;
	shift3  = disabled;	
}

bool Pizza::TOV::ADMBase::isEmpty() {
	return
		gxx     == disabled &&
		gxy     == disabled &&
		gxz     == disabled &&
		gyy     == disabled &&
		gyz     == disabled &&
		gzz     == disabled &&
		kxx     == disabled &&
		kxy     == disabled &&
		kxz     == disabled &&
		kyy     == disabled &&
		kyz     == disabled &&
		kzz     == disabled &&
		alp     == disabled &&
		shift1  == disabled &&
		shift2  == disabled &&
		shift3  == disabled;	
}


void Pizza::TOV::ADMBase::setStartFrom(int index) {
	gxx     =  0 + index;
	gxy     =  1 + index;
	gxz     =  2 + index;
	gyy     =  3 + index;
	gyz     =  4 + index;
	gzz     =  5 + index;
	kxx     =  6 + index;
	kxy     =  7 + index;
	kxz     =  8 + index;
	kyy     =  9 + index;
	kyz     = 10 + index;
	kzz     = 11 + index;
	alp     = 12 + index;
	shift1  = 13 + index;
	shift2  = 14 + index;
	shift3  = 15 + index;
	// assert count == 16
}


Pizza::TOV::HydroBase::HydroBase() {
	rho           = disabled;
	velx          = disabled;
	vely          = disabled;
	velz          = disabled;
	press         = disabled;
	eps           = disabled;
	temp          = disabled;
	efrac         = disabled;
}

bool Pizza::TOV::HydroBase::isEmpty() {
	return 
		rho           == disabled &&
		velx          == disabled &&
		vely          == disabled &&
		velz          == disabled &&
		press         == disabled &&
		eps           == disabled &&
		temp          == disabled && 
		efrac         == disabled;
}

void Pizza::TOV::HydroBase::setStartFrom(int index) {
	rho           =  0 + index;
	velx          =  1 + index;
	vely          =  2 + index;
	velz          =  3 + index;
	press         =  4 + index;
	eps           =  5 + index;
	temp          =  6 + index;
	efrac         =  7 + index;
	// assert count == 8
}


