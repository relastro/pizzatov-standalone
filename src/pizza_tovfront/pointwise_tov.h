#ifndef PIZZA_POINTWISE_TOV_FRONTEND
#define PIZZA_POINTWISE_TOV_FRONTEND

#include "pizza_tovlib/tovsol.h"
#include "pizza_tovlib/modes.h"
#include "pizza_tovfront/vectornames.h"
#include "pizza_tovfront/parameters.h"
#include "pizza_tovfront/idbase.h"
#include "pizza_tovfront/idvars.h"
#include <string>

namespace Pizza {
namespace TOV {


/**
 * Svens modification of Pizza::TOV::ptov which is heavily Cactus based to
 * a standalone version allowing pointwise evaluation.
 *
 **/
class pointwise_tov : public parameters, public idbase {
  tovsol star;

  // Variables for the perturbation:
  pmode_cowling *mode;
  double pert_scale;
  void perturb(vec_u& p, idvars& id) const;

  // And the kick:
  void kick(vec_u& p, idvars& id) const;

public:

  // Read-write: You MUST set these variables.
  HydroBase hydro;
  ADMBase adm;

  /**
   * The constructor does nothing. Thus you can have an instance of the class already
   * very early in your code.
   *
   * Using this instance, you want to change parameters as they appear in parameters.h
   * and want to set the variables hydro, adm and eos. Not setting the indices in hydro
   * or adm yields in an careful error. However, not setting the eos yields in an exception
   * during setting the star.
   **/
  pointwise_tov() {}
  
  /// doe the actual work
  void compute_star();

  /// Interpolate
  void initial_data(vec_u& x, idvars& id);
  void initial_data(const double* const Position3D, double* PrimitiveVector);

}; // class

} // ns
} // ns

#endif /* PIZZA_POINTWISE_TOV_FRONTEND */ 
