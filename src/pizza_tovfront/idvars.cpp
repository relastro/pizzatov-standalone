#include "pizza_tovfront/idvars.h"


using namespace Pizza;
using namespace Pizza::TOV;
using namespace std;

ostream& Pizza::TOV::operator<<(ostream &s,const idvars &id) {
	s<<endl;
	s<<"idvars holding:";
	s<<"glo="<< id.glo << endl;
	s<<"klo="<< id.klo << endl;
	s<<"rmd="<< id.rmd << endl;
	s<<"sed="<< id.sed << endl;
	s<<"press="<< id.press << endl;
	s<<"florentz="<< id.florentz << endl;
	s<<"vsqr="<< id.vsqr() << endl;
	s<<"florentz_from_vel="<< id.florentz_from_vel() << endl;
	
	s<<"lapse="<<id.lapse<<endl;
	s<<"shift="<<id.shift<<endl;
	s<<"end of idvars"<<endl;
	return s;
}

void idvars::copy(double* target, const ADMBase& adm, const HydroBase& hydro, const eos_1p& eos) {
	SkippableVector Q(target);
	
	// ADM
	
	Q[adm.alp] = lapse;
	Q[adm.shift1] = shift(0);
	Q[adm.shift2] = shift(1);
	Q[adm.shift3] = shift(2);
	
	Q[adm.gxx] = glo(0,0);
	Q[adm.gxy] = glo(0,1);
	Q[adm.gxz] = glo(0,2);
	Q[adm.gyy] = glo(1,1);
	Q[adm.gyz] = glo(1,2);
	Q[adm.gzz] = glo(2,2);
	
	Q[adm.kxx] = klo(0,0);
	Q[adm.kxy] = klo(0,1);
	Q[adm.kxz] = klo(0,2);
	Q[adm.kyy] = klo(1,1);
	Q[adm.kyz] = klo(1,2);
	Q[adm.kzz] = klo(2,2);

	// Basic primitives
	
	Q[hydro.rho] = rmd;
	Q[hydro.velx] = vel(0);
	Q[hydro.vely] = vel(1);
	Q[hydro.velz] = vel(2);
	Q[hydro.press] = press;
	Q[hydro.eps] = sed; // eos_barotropic.h, line 258
	
	// optional_hydro
	const double gm1 = eos.gm1_from_rmd(rmd);
	if(Q.wants(hydro.temp)) {
		// throws exception if not eos.has_temp()
		Q[hydro.temp] = eos.temp_from_gm1(gm1);
	}
	if(Q.wants(hydro.efrac)) {
		// throws exception if not eos.has_efrac()
		Q[hydro.efrac] = eos.efrac_from_gm1(gm1);
	}
}
