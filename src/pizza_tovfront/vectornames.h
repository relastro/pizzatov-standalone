#ifndef PIZZA_TOV_VECTORNAMES
#define PIZZA_TOV_VECTORNAMES

#include <stdio.h>

namespace Pizza {
namespace TOV {

/**
 * A VectorBase defines a mapping of numbers which are used for array indices.
 * 
 * This allows users to have any kind of array ordering in the case of vectors of unknowns,
 * ie. an array of sructures.
 * 
 * Usage example:
 * 
 *    // in your code using RNSID:
 *    ADMBase idx();
 *    idx.alp = 15;
 *    idx.shift1 = 19;
 *    // in the RNSID code himself:
 *    double myUnknowns[100];
 *    myUnknowns[idx.alp] = compute_alp();
 *    myUnknowns[idx.shift1] = compute_shift();
 * 
 **/
class VectorBase {
public:
	/**
	 * Magic number indicating that the index shall not be set.
	 * All indices shall be disabled by default in nested classes,
	 * this is the job of the constructors.
	 **/
	static const int disabled = -1;
	
	static void disable(int& index) {
		index = disabled;
	}
};


/**
 * A vector class which acts as a transparent write mask
 *
 * Using this class, you basically get an array with a default storage for all
 * negative indices. This is used for masking when writing to the array. It is
 * not useful to use this instance for reading.
 *
 * Usage example:
 *    ADMBase idx;
 *    idx.alp = 15;
 *    idx.shif1 = idx.disabled;
 *    const double storage[100]; 
 *    SkippableVec V(storage);
 *    V[idx.alp] = 3.14;
 *    V[idx.shif1] = 4.25;
 * 
 * In this example, the number 4.25 is never stored when thinking of idx as something
 * users "request".
 * 
 * You can also make read access to V[...], but in this particular example, reading
 * V[idx.shift1] makes no sense.
 *
 * This class is not really thread safe: There is one dummy double where the access reference
 * is going to in case of masking. In principle it does not hurt if multiple threads access
 * for writing at the same time but it is awkward.
 **/
struct SkippableVector {
	double *Sparse;
	double dummyTarget;
	SkippableVector(double* Sparse_) : Sparse(Sparse_) {}
	
	bool wants(const int index) { return index != VectorBase::disabled; }
	double& access(const int index) {
		//printf("Set %d: %s, aryval=%e dummy=%e\n", index, wants(index)?"Yes":"No", Sparse[index], dummyTarget);
		return wants(index) ? Sparse[index] : dummyTarget;
	}
	double& operator [] (const int index) { return access(index); }
	double& operator () (const int index) { return access(index); }
};

/**
 * The ADMBase stores indices for the 3-metric, 3-curvature, lapse and shift.
 *
 * The name is the same as the Cactus ADMBase thorn just for fun.
 **/
class ADMBase : public VectorBase {
public:
	int gxx; 
	int gxy; 
	int gxz;
	int gyy;
	int gyz; 
	int gzz;
	
	int kxx; 
	int kxy; 
	int kxz;
	int kyy;
	int kyz; 
	int kzz;
	
	int alp;
	int shift1;
	int shift2;
	int shift3;
	
	static const int count = 16;
	
	/**
	 * Determines whether all fields have been set to
	 * disabled (the default value).
	 **/
	bool isEmpty();
	
	/**
	 * A quick way to quickly assign all entries
	 * uprising numbers, starting from index.
	 **/
	void setStartFrom(int index=0);
	
	/**
	 * By default, all targets are skipped
	 **/
	ADMBase();
};

/**
 * The HydroBase stores indices for the conserved and primitive values of the
 * hydrodynamics. It also stores eps but does not store any magnetic field or so.
 * 
 * The name is the same as the Cactus HydroBase thorn just for fun. *
 **/
class HydroBase  : public VectorBase {
public:
	// primitives
	int rho;
	int velx;
	int vely;
	int velz;
	int press;
	
	// further hydro quantities
	int eps;
	int temp;
	int efrac;
	
	static const int count = 8;
	
	/**
	 * Determines whether all fields have been set to
	 * disabled (the default value).
	 **/
	bool isEmpty();
	
	/**
	 * A quick way to quickly assign all entries
	 * uprising numbers, starting from index.
	 * Typically you don't want this in production as
	 * you'll never store both the primitives and the conserved
	 * variables.
	 **/
	void setStartFrom(int index=0);

	/**
	 * By default, all targets are skipped
	 **/
	HydroBase();
};


} // ns TOV
} // ns Pizza

#endif /* PIZZA_TOV_VECTORNAMES */
