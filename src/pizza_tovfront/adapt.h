#ifndef PIZZA_TOVFRONT_ADAPT
#define PIZZA_TOVFRONT_ADAPT

#include "pizza_tovlib/unitconv.h"

namespace Pizza {
namespace TOV {
	

/**
 * The adapt is one of the relics from the PizzaBase. There they also have the mesh
 * with all its crazy definitions which we just omit here in order to keep code smallish.
 *
 * The only thing this structure does is the unit conversion between Cactus geometric
 * units and Pizza SI units. This adapter hardcodes the geometric units so users should
 * never really see Pizzas internal units.
 * 
 *
 **/
struct adapt {
	// Geometric unit system for initial data, specified by length unit.
	// use CACTUS units
	static constexpr double length_unit = 1476.7161818921163;
	
	units internal_units;
	
	adapt() {
		internal_units = units::geom_ulength(length_unit);
	}

};

} // ns TOV
} // ns Pizza

#endif /*PIZZA_TOVFRONT_ADAPT*/
