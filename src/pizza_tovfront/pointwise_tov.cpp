#include "pizza_tovfront/pointwise_tov.h"
#include "pizza_tovlib/error.h"
#include <iostream>
#include <stdio.h>
#include <cmath>
#include <stdexcept>

using namespace Pizza;
using namespace Pizza::TOV;
using namespace std;

void pointwise_tov::compute_star() {
	// prepare for the interpolation:assure all mandatory stuff is set
	error::incase(hydro.isEmpty() || adm.isEmpty(), "You want to query at least one output field");
	
	double rmd_c = star_crmd / internal_units.density();
	// any access to an unitialized eos will yield in an exception.
	star  = tovsol(rmd_c, eos, 0.0, 1e-10, star_gm1_cut);
	
	// prepare the perturbation
	if (has_pert()) {
		mode = new pmode_cowling(star, pert_l, pert_m, pert_n, 1e-5);
		double vmean = sqrt(2.0*mode->kin_energy()/star.grav_mass());
		pert_scale = pert_amp / vmean;
	}

	// Print something about the star:
	std::cout << star.to_str();
}

/**
 * Implementation stemming from ptov::init_vars
 *
 * For this interface here, we need the idvars.
 * 
 **/
void pointwise_tov::initial_data(vec_u& x, idvars& id) {
	const double rc           = x.norm();
	const tov_vars tv         = star(rc);
	const double h            = tv.g_rr();
	const double fbr2         = 2.0 * h * tv.m_by_r3;
	
	// we only support cartesian coordinates here.
	id.glo = ONE;
	for (int i=0;i<3;i++) {
		for (int j=0;j<=i;j++) {
			id.glo(i,j) += x(i) * x(j) * fbr2;
		}
	}

	id.klo      = ZERO;
	id.lapse    = tv.lapse();
	id.shift    = ZERO;

	id.rmd      = tv.rmd;
	id.sed      = eos.sed_from_gm1(eos.gm1_from_rmd(tv.rmd));
	id.press    = tv.p;
	id.vel      = ZERO;
	id.florentz = 1.0;
	
	if(has_pert())  perturb(x, id);
	if(has_kick())  kick(x, id);
}

void pointwise_tov::initial_data(const double* const pos, double* Q) {
	vec_u x;
	x(0) = pos[0];
	x(1) = pos[1];
	x(2) = pos[2];
	
	idvars id;
	initial_data(x, id);
	
	// Debugging: Print out the initial data:
	//std::cout << "ID at: " << x << std::endl;
	//std::cout << id << std::endl;
	
	id.copy(Q, adm, hydro, eos);
}

void pointwise_tov::perturb(vec_u& p, idvars& id) const {
	eos_cold eosc = eos;
	vec_u er,eth,ephi;
	double r,th,phi;
	// coord_spherical(p, r, th, phi):
        const double d  = sqrt(sqr(p(0))+sqr(p(1)));
        r               = p.norm();
        th              = ((d != 0) || (p(2) != 0)) ? atan2(d, p(2)) : 0.0;
        phi             = ((p(0) != 0) || (p(1) != 0)) ? atan2(p(1), p(0)) : 0.0;
	pmode_cowling::pert_local pert = (*mode)(r, th, phi);

	// assuming cartesian coord_type()
	er(0)   = sin(th)*cos(phi);
	er(1)   = sin(th)*sin(phi);
	er(2)   = cos(th);

	eth(0)  = cos(th)*cos(phi);
	eth(1)  = cos(th)*sin(phi);
	eth(2)  = -sin(th);

	ephi(0) = -sin(phi);
	ephi(1) = cos(phi);
	ephi(2) = 0;

	id.vel  += (pert.dvr.imag() * er + pert.dvth.imag() * eth + pert.dvphi.imag() * ephi)*pert_scale;

	double hm1 = eosc.hm1_from_rmd(id.rmd);
	hm1         = max(0.0, hm1 + pert.dh.imag() * pert_scale);
	id.rmd      = eosc.rmd_from_hm1(hm1);
	id.sed      = eosc.sed_from_hm1(hm1);
	id.press    = eosc.p_from_hm1(hm1);
	error::incase(id.vsqr() >= 0.99999, "TOV: velocity perturbation too large, v>=c");
	id.florentz = id.florentz_from_vel();
}


void pointwise_tov::kick(vec_u& p, idvars& id) const {
	double amp = kick_amp / star.radius();
	// only supporting coord_type() == cartesian here.
	if (p.norm() < star.radius()) {
		id.vel  += p * amp;
	}
	error::incase(id.vsqr() >= 0.99999, "TOV: radial kick too large, v>=c");
	id.florentz = id.florentz_from_vel();
}

