PizzaTOV in ExaHyPE
===================

PizzaTOV is an initial data solver for nonrotating stars in equilibrium
(solutions of the TOV equations). In contrast to RNSID, the PizzaTOV code
can do

 * Well defined perturbations in terms of pressure modes in Cowling approximation
 * Kicks (boosts) in a certain direction

The PizzaTOV code can also create "crappy" initial data for binary neutron
stars. They have been used for instance to setup eccentric mergers.

In ExaHyPE, we need PizzaTOV because well defined perturbations are the starting
point to do any kind of convergence study in TOV systems.

The story of Pizza (in Cactus)
------------------------------

So finally Pizza made its way into ExaHyPE. Pizza is the infamous C++ code
doing all the initial data and equation of state infrastructure for WhiskyTHC.
It is a very sophisticated C++ codebase which is quite difficult to read.
Most classes are very small and undocumented and there is code doing everything,
even the linear algebra and basic types. Massive operator overloading everywhere
makes it difficult to understand what's going on, especially in Cactus where
the code is streched over several apparently independent modules (thorns).

PizzaTOV standalone: Code overview
----------------------------------

In contrast, the Pizza codebase in this directory is completely standalone. It
consists of two modules:

* The `pizza_tovlib` which is 90% the codebase of the standalone `tovsolver`
  executable in the THCSupport repository. The other 10% are Pizza headers
  from other thorns, such as the linear algebra.
* The `pizza_tovfront` is handcrafted from the `PizzaTOV` thorn and the various
  surrounding thorns such as `PizzaIDBase`. In contrast to the cactus version,
  this module is really understandable and documented and has only direct
  dependencies to the `pizza_tovlib` code.

Demonstrator
------------

There are three executables in the `src` folder:

* In the `tovstar_dump` directory, `tovstar` and `tovstars` which are indeed
  the executables from the Pizza standalone `tovsolver`. They can be used to
  generate sequences of stars on its own.
* In the `cpp-standalone` directory, the `demonstrator` executable which
  shows how to use the PizzaTOV pointwise frontend.

System dependencies for Pizza
-----------------------------

In order to compile Pizza, you need

* GSL
* BOOST

The BOOST dependency may be annoying as BOOST is such a large codebase. I tried
to get rid of it, replacing certain calls with modern C++11 equivalents (for
instance the `foreach` iterator loops). The only remaining BOOST dependency is on

* boost format strings (everywhere where tabulated output can be created)
* boost string library (in the `eos_barotropic_file.cpp` file parser)
* boost filesystem and parameter library (only in the `tovstar_dump` programs)

That is, in principle all BOOST dependencies in the library part are header only,
thus there is no need to include BOOST on linking. This fact reflects in the
exemplary Makefile which compiles the standalone cpp_demonstrator.

Equation of State files
-----------------------

Pizza ships a rich infrastructure to handle all kind of equations of states. This
includes a dedicated file format to write down equations of states. The directory
`example_eos_files` contains such files. It's up to the user to stick to the EOS
files when reading in the equation of state or to directly use the API to set up
the EOS in code. In Cactus, we didn't have this choice. This is why I implemented
the possibility to read in an EOS file from string -- a remnant which is also
present in this code base and allows to embed the EOS in some other parameter
format.

Note that some parts, such as the perturbation, only work with a cold equation of
state, such as a polytropic EOS.


A note about Open Source
------------------------

The Pizza code was written by Wolfgang Kastaun. It is said that this code is
closed source. I'm not sure.

All code written by SvenK in this directory is public domain for the sake of
simplicity.


-------

Author: Sven Köppel, 2017-04-28